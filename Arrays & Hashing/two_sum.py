def twoSum(self, nums: List[int], target: int) -> List[int]:
    output=[]
    # for i, num in enumerate(nums):
    #     for j, numinner in enumerate(nums):
    #         if num + numinner == target and i != j:
    #             return [i, j]


    #Iterate through list of nums
    for i, n in enumerate(nums):
        #hold current index in output
        output.append(i)
        #finding the number that would give the target

        pair = target-n
        #if the pair is the same value as n, find index for next instance of n
        if i < len(nums)-1 and pair == n:
            try:
                output.append(nums.index(pair, i+1))
                return output
            except:
                output=[]
                continue

        #check if that number is inside the list
        if pair in nums:
            #add the index of that number to the output
            ansindex = nums.index(pair)
            output.append(ansindex)
            return output

        output = []
