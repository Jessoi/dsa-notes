def isAnagram(s: str, t: str) -> bool:
    count = defaultdict(int)
    # Count the frequency of characters in string s
    for c in s:
        count[c] += 1
    # Decrement the frequency of characters in string t
    for c in t:
        count[c] -= 1
    # Check if any character has non-zero frequency
    for val in count.values():
        if val != 0:
            return False

    return True